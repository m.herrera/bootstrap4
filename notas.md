
# Docs bootstrap

STOP: video 33
## Páginas útiles
paleta de colores rgba
- https://rgbacolorpicker.com/
- coolors.co

imágenes
- freepik
- undraw.io

plantillas
- html5 up

optimizar tamaño de imágenes
- https://squoosh.app/

redimensionar tamaño de imágen
- iloveimg 

* para ignorar archivos, creamos el archivo .gitigore y ahi ponemos los nombres/carpetas
  
## Overflow
overflow-auto habilita un scroll en el contenido
overflow-hidden oculta el contenido

## Posicionamiento
* position-absolute se posiciona desde su contenedor padre
* position-relative se posiciona desde donde estaba
* position-fixed se posiciona el el 0,0 del navegador; y siempre esta flotando en los demas elementos
* sticky-top sirve para posicionar los menus
* fixed-top posiciona los menus